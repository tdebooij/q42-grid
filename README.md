# q42-grid

Assignment for q42.

Create a 50x50 grid, that adds 1, and highlights yellow to each cell/row on an intersection when clicked. When at least 5 in a row are a part of the fibonacci series, highlight those in green and clear them back empty.

[Demo](https://tdebooij.gitlab.io/q42-grid/)

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
