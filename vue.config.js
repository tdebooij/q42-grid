module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/q42-grid/'
    : '/'
}