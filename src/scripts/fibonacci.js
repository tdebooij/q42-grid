/**
 * The golden ratio (φ)
 */
const phi = (1 + Math.sqrt(5)) / 2;

/**
 * Checks if n consecutive numbers in an array (of int) are in (consecutive) fibonacci order
 * @param  { [Number] } sequence - The sequence to check in
 * @param  { Number } length - The amount of consecutive fibonacci numbers
 *
 */
function isFibonacciSequence(sequence, minLength) {
  // Check the inputs
  if (sequence.length < minLength) {
    console.error("The sequence should be at least the size of minLength");
    return null;
  }

  if (sequence.length < 3) {
    console.error("The sequence should contain at least 3 numbers");
    return null;
  }

  let sequenceStart = 0;
  let sequenceLength = 0;

  let sequencesFound = Array();

  // Loop over each value in the sequence array, start at the 3rd value
  for (let i = 2; i < sequence.length; i++) {
    // Check if the previous two numbers are in order, in fibonacci AND add up to the current number
    if (
      sequence[i - 2] !== null &&
      sequence[i - 1] >= sequence[i - 2] &&
      isFibonacciNumber(sequence[i - 2]) &&
      isFibonacciNumber(sequence[i - 1]) &&
      sequence[i - 2] + sequence[i - 1] === sequence[i]
    ) {
      // We have found a sequence!
      if (sequenceLength === 0) {
        // If the is a new sequence, start counting at three
        sequenceLength = 3;
      } else {
        // If we already have a sequence, add one to that!
        sequenceLength++;
      }
    } else {
      // No sequence, Reset the counters
      sequenceLength = 0;
      sequenceStart = i - 1;
      continue;
    }

    if (sequenceLength === minLength) {
      // If we have found a sequence of exactly the minimun length, push a new return object to the sequences found
      sequencesFound.push({
        startIndex: sequenceStart,
        sequenceLength: sequenceLength
      });
    } else if (sequenceLength > minLength) {
      // If we have exceeded the length, set the length of the last sequencesfound entry to the current length
      sequencesFound[sequencesFound.length - 1].sequenceLength = sequenceLength;
    }
  }

  return sequencesFound;
}

/**
 * Returns the next fibonacci number after the given one by using the golden ratio and rounding
 * See: https://en.wikipedia.org/wiki/Fibonacci_number#Relation_to_the_golden_ratio
 * WARNING: Does not work correctly when the input number is ONE, it will return 2, while in
 * actuality it can be either 1 OR 2
 * @param  { Number } number - The current fibonacci number
 * @returns { Boolean } - The next number in the fibonacci sequence, or -1 if not a fibonacci number
 */
// eslint-disable-next-line no-unused-vars
function getNextFibonacciNumber(number) {
  // Handle edge case where number is 0;
  if (number === 0) return 1;

  if (!isFibonacciNumber(number)) return -1;
  return Math.round(number * phi);
}

/**
 * Checks if a given number is in the fibonacci sequence
 * See: https://en.wikipedia.org/wiki/Fibonacci_number#Identification
 * @param  { Number } number - The number to check
 * @returns { Boolean } - true if the number is in the fibonacci sequence, false otherwise
 */
function isFibonacciNumber(number) {
  return (
    isPerfectSquare(5 * Math.pow(number, 2) + 4) ||
    isPerfectSquare(5 * Math.pow(number, 2) - 4)
  );
}

/**
 * Checks if a given number is a perfect square
 * See: https://en.wikipedia.org/wiki/Square_number
 * @param  { Number } number - The number to check
 * @returns { Boolean } true if the number is a perfect square, false otherwise
 */
function isPerfectSquare(number) {
  return number >= 0 && number % Math.sqrt(number) === 0;
}

export default {
  isFibonacciSequence
};
