class Gridje {
  /**
   * Initialize the grid, for example
   * new Gridj(3) will create the following 2d array
   *         1     2     3       y
   * 1 [ [ null, null, null ] ]
   * 2 [ [ null, null, null ] ]
   * 3 [ [ null, null, null ] ]
   * x
   * @param  {} gridSize
   */
  constructor(gridSize) {
    this.gridSize = gridSize;

    // Initialize the grid
    this.gridArray = Array(gridSize);

    // Loop over the gridsize to assign a new array to each entry
    for (let x = 0; x < gridSize; x++) {
      this.gridArray[x] = new Array(gridSize);

      // Loop over each nested array and assign null to the value
      for (let y = 0; y < gridSize; y++) {
        this.gridArray[x][y] = null;
      }
    }
  }

  /**
   * Fire a callback for each cell in either the given row or column
   * @param  { number } row - the row to fire the callback on
   * @param  { number } col - the column to fire the callback on
   * @param  { function } callback - the callback function
   */
  forEachCellInInterSection(row, col, callback) {
    for (let i = 0; i < this.gridSize; i++) {
      this.gridArray[row][i] = callback(this.gridArray[row][i], row, col);
      // Skip the update if the iterator is the same as the row value,
      // we've updated this cell in the previous statement already
      if (i !== row) {
        this.gridArray[i][col] = callback(this.gridArray[i][col], row, col);
      }
    }
  }

  /**
   * Fire a callback for each cell in either the given row
   * @param  { number } row - the row to fire the callback on
   * @param  { function } callback - the callback function
   */
  forEachCellInRow(row, callback) {
    // Loop over each cell in the given row
    for (let i = 0; i < this.gridSize; i++) {
      this.gridArray[row][i] = callback(this.gridArray[row][i]);
    }
    return this.gridArray[row];
  }

  /**
   * Fire a callback for each cell in either the given column
   * @param  { number } col - the column to fire the callback on
   * @param  { function } callback - the callback function
   */
  forEachCellInColumn(col, callback) {
    // Create an empty array, to return the column in.
    let updatedColumn = Array();

    // Loop over each row, and fire the callback for each cell in the 'row' index
    for (let i = 0; i < this.gridSize; i++) {
      this.gridArray[i][col] = callback(this.gridArray[i][col]);

      //Add the updated cell to the return array
      updatedColumn.push(this.gridArray[i][col]);
    }

    return updatedColumn;
  }

  /**
   * Returns a clone of the instantiated class.
   */
  clone() {
    return Object.assign(Object.create(Object.getPrototypeOf(this)), this);
  }
}

export let gridje = Gridje;
